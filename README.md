# [fotografii.org](https://fotografii.org) source codes

<br/>

### Run fotografii.org on localhost

    # vi /etc/systemd/system/fotografii.org.service

Insert code from fotografii.org.service

    # systemctl enable fotografii.org.service
    # systemctl start fotografii.org.service
    # systemctl status fotografii.org.service

http://localhost:4065
